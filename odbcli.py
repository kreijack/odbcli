"""
odbcli - command line interface for pyodbc
Copyright (C) 2020 Goffredo Baroncelli <kreijack@inwind.it>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import sqlite3
import sys
import pprint
import datetime
import pyodbc
import configparser
import os
import atexit

class Main:
    def __init__(self):
        self._conf=None

        self._sql_commands = [
            'ADD', 'ALTER TABLE', 'AND', 'AS', 'ASC', 'AVG', 'BETWEEN',
            'CASE', 'COUNT', 'CREATE TABLE', 'DELETE', 'DELETE FROM', 'DESC',
            'ELSE', 'END', 'FROM', 'GROUP BY', 'HAVING', 'HAVING COUNT',
            'INNER JOIN', 'INSERT', 'INSERT INTO', 'IS NOT NULL', 'IS NULL',
            'JOIN', 'LEFT JOIN', 'LIKE', 'LIMIT', 'MAX', 'MIN', 'NULL', 'ON',
            'OR', 'ORDER BY', 'OUTER JOIN', 'ROUND', 'SELECT', 'SELECT AVG',
            'SELECT COUNT', 'SELECT DISTINCT', 'SELECT MAX', 'SELECT MIN',
            'SELECT ROUND', 'SELECT SUM', 'SET', 'SQL', 'SUM', 'THEN',
            'UPDATE', 'VALUES', 'WHEN', 'WHERE', 'WITH',

            ".help", ".autocommit", ".bye", ".list-profiles",
            ".change-profile ", ".header ", ".output ",
            ".output_once ", ".tables", ".columns "
        ]

        self._conn = None
        self._cursor = None
        self._autocommit = False
        self._header = True
        self._output = None
        self._output_once = False

    def init_config(self):
        self._conf = configparser.ConfigParser()
        self._conf.read_file(open("odbcli.ini"))

        self._profile = self._conf.get("DEFAULT", "defaultprofile")
        for arg in sys.argv[1:]:
            if arg.startswith("--profile="):
                self._profile=arg[10:]

    def open_connection(self, profile=None):

        server = self._conf.get(self._profile, "server")
        database = self._conf.get(self._profile, "database")
        username = self._conf.get(self._profile, "username")
        password = self._conf.get(self._profile, "password")
        driver = self._conf.get(self._profile, "driver")

        cs = 'DRIVER={%s};SERVER={%s};DATABASE={%s};UID={%s};PWD={%s}'%(
            driver, server, database, username, password
        )

        print("Username:   ", username)
        print("Database:   ", database)
        print("Server:     ", server)
        print("Driver:     ", driver)
        print("Autocommit: ", self._autocommit)
        print("Profile:    ", self._profile)

        self._conn = pyodbc.connect(cs, autocommit=self._autocommit)
        self._curs = self._conn.cursor()

    #from https://pymotw.com/3/readline/
    def init_readline(self):
        try:
            import gnureadline as readline
        except ImportError:
            import readline
        import logging

        class SimpleCompleter:

            def __init__(self, options):
                self.options = sorted(options)

            def complete(self, text, state):
                response = None
                if state == 0:
                    # This is the first time for this text,
                    # so build a match list.
                    if text:
                        self.matches = [
                            s
                            for s in self.options
                            if s and (s.startswith(text.upper()) or
                                      (s.startswith(".") and s.startswith(text)))
                        ]
                    else:
                        self.matches = self.options[:]

                # Return the state'th item from the match list,
                # if we have that many.
                try:
                    response = self.matches[state]
                except IndexError:
                    response = None
                return response

        # Register the completer function
        readline.set_completer(SimpleCompleter(self._sql_commands).complete)

        # Use the tab key for completion
        readline.parse_and_bind('tab: complete')

        hf = self._conf.get("DEFAULT", "historyfile")
        if os.path.exists(hf):
            readline.read_history_file(hf)

        atexit.register(readline.write_history_file, hf)

    def parse_internal_command(self, cmd):

        if cmd == ".bye":
            print("Bye...")
            sys.exit(0)

        elif cmd == ".autocommit":
            self._autocommit = not self._autocommit
            self.open_connection()

        elif cmd.startswith(".header"):
            value = cmd[8:]
            if len(value):
                self._header = not (value == "off")
            if self._header:
                print("Header enabled")
            else:
                print("Header disabled")

        elif cmd.startswith(".output_once"):
            value = cmd[12:]
            if len(value) < 2:
                self._output = None
                print("Set output to stdout")
            else:
                self._output = value[1:]
                self._output_once = True
                print("Append output (once) to file '%s'"%(self._output))

        elif cmd.startswith(".output"):
            value = cmd[7:]
            if len(value) < 2:
                self._output = None
                print("Set output to stdout")
            else:
                self._output = value[1:]
                print("Append output to file '%s'"%(self._output))

        elif cmd == ".list-profiles":
            for s in self._conf.sections():
                print("%s"%(s))

        elif cmd.startswith(".change-profile "):
            self._profile = cmd[16:]
            self.open_connection()

        elif cmd == ".tables":
            header = [
                "table_cat", "table_schem", "table_name", "table_type"
            ]
            self.dump_table(self._curs.tables(), header)

        elif cmd.startswith(".columns "):
            value = cmd[9:]
            if len(value) < 1:
                return

            header = ["table_cat", "table_schem", "table_name",
              "column_name", "data_type", "type_name", "column_size",
              "buffer_length", "decimal_digits", "num_prec_radix",
              "nullable", "remarks", "column_def", "sql_data_type",
              "sql_datetime_sub", "char_octet_length", "ordinal_position",
              "is_nullable"
            ]

            self.dump_table(self._curs.columns(value), header)

        elif cmd == ".help":
                print("""
.bye                        exit from this shell
.help                       show this help
.autocommit                 toggle autocommit
.list-profiles              list the availables profiles
.change-profile <profile>   change the current profile
.header [on|off]            enable|disable the header
.output [<filename>]        set the output to the file <filename>
.output_once [<filename>]   set the next cmd output to the file <filename>
.tables                     show the database tables
.columns <tablename>        show the table structure
""")

        else:
            print("Unknown dot-command '%s'"%(cmd))

    def main(self):

        self.init_config()
        self.init_readline()
        self.open_connection()
        cmd = ""
        while True:
            if len(cmd):
                prompt = "| "
            else:
                prompt = "> "

            try:
                l = input(prompt) #sys.stdin.readline()
            except EOFError:
                print()
                print("Bye...")
                break

            cmd += l
            scmd = cmd.strip()
            if scmd.startswith("."):
                self.parse_internal_command(cmd)
                cmd = ""
            elif scmd.endswith(";"):
                try:
                    c1 = cmd
                    cmd = ""
                    self._curs.execute(c1)
                except pyodbc.ProgrammingError as e:
                    print("Error: '%s'"%(e))
                    continue


                self.dump_table(self._curs.fetchall(),
                    [column[0] for column in self._curs.description])

    def dump_table(self, iterator, header):

        if self._output:
            f = open(self._output, "a")
        else:
            f = sys.stdout

        if self._header:
            f.write("\t".join(header))
            f.write("\n")

        cnt = 0
        for row in iterator:
            f.write("\t".join(map(str, row)))
            f.write("\n")
            cnt += 1

        if self._header:
            f.write("Fetched %d rows.\n"%(cnt))

        if self._output:
            f.close()
        if self._output_once:
            self._output_once = False
            self._output = None


Main().main()
