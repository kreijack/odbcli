### Introduction

This is a small python script that create a simple SQL shell around the
pyodbc libray.

This shell works like similar shell (e.g. the one provided by sqlite
database library).


### Usage

The program is run doing:
```
$ python odbcli.py
```

Then on the basis of the odbcli.ini file, it connect the database server and
allow the user to execute directly some SQL commands. The SQL command must
be ended by '**;**'. A command may span several lines.

As othe shell, there are some *dot* command which are execute internally.
Below the main '**dot**' commands:
- **.bye**: exits from the program
- **.help**: show the list of the '**dot**' commands
- **.list-profiles**: show the list of the availables profiles
- **.change-profile <profile>**: change to the profile <profile>
- **.autocommit**: toggle the autocommit mode (default off)

### readline library

This script uses the readline library and its faciclities:
- the line can be edited
- the user can go to the past inserted lines
- the history file is save at the exit of the program
- there is the autocompletametion of the SQL command (case insensitive) and
  'dot' command (case sesistive)

### Config ini file
This script uses the configuration information stored in the 'odbcli.ini'
ini file. Below an example of this file:

```
[DEFAULT]
historyfile=.odbcli.history
defaultprofile=SQLSERVERBASE

[SQLSERVERADMIN]
server=tcp:127.0.0.1
database=bombrowser
username=SA
password=Pippo123.
driver=ODBC Driver 17 for SQL Server

[SQLSERVERBASE]
server=tcp:127.0.0.1
database=bombrowser
username=bombrowserUser
password=bombrowserPassword1.
driver=ODBC Driver 17 for SQL Server
```

In this file there is asection called "DEFAULT" which contains:
- **historyfile**: the history file name
- **defaultprofile**: the default profile to use

Then the next sections are the so called 'profile' section. In each 'profile'
section are store the following information:
- **server**: the server to connect
- **database**: the name of the database
- **username**: the user name for the connection
- **password**: the password for the connection
- **driver**: the *ODBC* driver to use
